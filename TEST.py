import requests
from bs4 import BeautifulSoup

url = "http://erpmetal.mirea.ru/img/2/10"

headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Max-Age': '3600',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
}

if __name__ == '__main__':
    req = requests.get(url, headers)
    soup = BeautifulSoup(req.content, 'html.parser')
    res = soup.find_all("a")
    for r in res:
        href = r.get('href')
        if ('jpg' or 'png') in href:
            href = '{}/{}'.format(url, href)
            print(href)

    # path_tube = r'D:\Git_project\recorgnition-scrap-web\static\TEST_TUBES'
    # coordinates = load_data(os.listdir(path_tube), TEST_FOLDER_TUBE, path_model=r'D:\Git_project\recorgnition-scrap-web\static\models_segmentation\50_best_model.h5')
    # print(coordinates)
    # result = draw_circles_images(coordinates)
    # print(result)

    # path_scrap = r'D:\Git_project\recorgnition-scrap-web\static\TEST\1_4.png'
    # image = cv2.imread(path_scrap)
    # out_return = get_segmentation(image, None, checkpoint_path=MODELS_SEGMENTATION,color=(23, 185, 247))
    # print(out_return)
    # path_test = r'common/1.png'
    # image = cv2.imread(path_test)
    # res = include_area(image)
    # cv2.imwrite('test.png', res)
    # crop_by_mask_black('3.png', 'test.png', 'out.png', exclude_mask=True)

    # mix_path = r'D:\Git_project\recorgnition-scrap-web\static\TEST_CLASS_SEG\crop_87_1.png'
    # out_return = predict(model=None, inp=mix_path,
    #                      out_fname='test.png',
    #                      checkpoints_path=r'D:\Git_project\recorgnition-scrap-web\static\models_segmentation\segnet-6_class_1024x1024')
    #
    # flatten = out_return.ravel()
    # res = np.unique(flatten, return_counts=True)
    # print(res)
    # print(calculate_average_segment(out_return))

    # crop_by_mask(r'D:\Git_project\recorgnition-scrap-web\static\TEST\38_4.png',
    #              r'D:\Git_project\recorgnition-scrap-web\static\TEST\out_36_4.png', 'test1.png', cnts=False,
    #              exclude_mask=True)

    # flatten = out_return.ravel()
    # res = numpy.unique(flatten, return_counts=True)
    # print(res[1], flatten.shape[0])
    # mean_result = list(map(lambda x: int((x / flatten.shape[0]) * 100), res[1]))
    # print(mean_result)
    # print(collections.Counter(out_return.ravel()))

    # # path = r'D:\Git_project\wer_rec_scrap\orig\27_4.png'
    # # out = r'./static/ts'
    # images = os.listdir('crop')
    # images = [os.path.join('crop',im) for im in images]
    # # a = seg.get_prediction(r'D:\Git_project\wer_rec_scrap\orig\27_4.png',out ,MODELS_SEGMENTATION)
    # # result = seg.crop_by_mask(path,a,'test.png ')
    # model = load_model(r'static/model_classification/ModelGlobal-12-0.87.hdf5', custom_objects={'f1': f1})
    # times = time.time()
    # result = predict_multiple_class_scrap(images, model)
    # print(result)
    # print(time.time() - times)
    #

    # out_pred, batch_img = get_multiple_segmentation_class(test_image_seg_class, TEST_IMAGE_FOLDER_CLASS_SEG,
    #                                              MODELS_SEGMENTATION_CLASS, is_test=True,test_folder=TEST_CLASS_SEG)
    # print(out_pred, batch_img)
    # images = os.listdir('test/test_image')
    # seg.get_multiple_segmentation(images,'test/test_image',MODELS_SEGMENTATION)
    # seg.crop_by_mask(r'static/image/out_2_0_color.jpg',r'out_2_0_color')
