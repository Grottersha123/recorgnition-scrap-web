import os

import flask_monitoringdashboard as dashboard
from flask import Flask, render_template, request, jsonify, url_for
from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_bootstrap import Bootstrap
from flask_cors import CORS
from flask_login import current_user
from flask_security import SQLAlchemySessionUserDatastore, Security, login_required, roles_accepted
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename, redirect

from clean_project import clean_data
from config import ALLOWED_EXTENSIONS, conf_update, Config

app = Flask(__name__,
            static_folder="./static",
            template_folder="./templates")

cors = CORS(app)

dashboard.bind(app)
app.templates_auto_reload = True
app.config.from_object(Config)
# app.config.from_object(ProductionConfig)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config.update(conf_update)

from flask_admin import Admin


# Flask and Flask-SQLAlchemy initialization here


# api = Api(mod)
# CORS(app)
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


db = SQLAlchemy(app)

from tube_app.models import SteelGrade, Tube, PipeSize, CountTube, Journal
from scrap_app.models import Train, ScrapLayer, Statistic
from models import *




class AdminMixin:
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login', next=request.url))


class AdminView(AdminMixin, ModelView):
    pass


class HomeAdminView(AdminMixin, AdminIndexView):
    pass


class BaseModelView(ModelView):
    def on_model_change(self, form, model, is_created):
        # print(model)
        # model.generate_token()
        return super(BaseModelView, self).on_model_change(form, model, is_created)


class UserAdminView(AdminMixin, BaseModelView):
    # email = db.Column(db.String(100), unique=True)
    #     password = db.Column(db.String(255))
    #     active = db.Column(db.Boolean())
    #     roles = db.relationship('Role',
    #                             secondary=roles_users,
    #                             backref=db.backref('users', lazy='dynamic'))
    #     token = db.Column(db.String(255))
    form_columns = ['email', 'password', 'password_a', 'active', 'roles', 'token', 'name', 'surname', 'last_name']


admin = Admin(app, name='scrap_app', template_mode='bootstrap3',
              index_view=HomeAdminView(name='Home'))

admin.add_view(ModelView(SteelGrade, db.session))
admin.add_view(ModelView(PipeSize, db.session))
admin.add_view(ModelView(Tube, db.session))
admin.add_view(ModelView(CountTube, db.session))
admin.add_view(ModelView(Train, db.session))
admin.add_view(ModelView(ScrapLayer, db.session))
admin.add_view(ModelView(Statistic, db.session))
admin.add_view(UserAdminView(User, db.session))
admin.add_view(ModelView(Role, db.session))
admin.add_view(ModelView(Journal, db.session))

from tube_app.forms import ExtendedRegisterForm
# Flask-security


user_datastore = SQLAlchemySessionUserDatastore(db.session, User, Role)
security = Security(app, user_datastore, login_form=ExtendedRegisterForm)

from tube_app.views import tubes
from scrap_app.views import scrap

app.register_blueprint(scrap)
app.register_blueprint(tubes)

Bootstrap(app)

clean_data(app.config['UPLOAD_FOLDER'])


def upload_file():
    orig_files = []
    if 'files[]' not in request.files:
        resp = jsonify({'message': 'No file part in the request'})
        resp.status_code = 400
        return resp
    files = request.files.getlist('files[]')
    if files == '':
        resp = jsonify({'message': 'No file selected for uploading'})
        resp.status_code = 400
        return resp
    for file in files:
        if file.filename == '':
            resp = jsonify({'messag e': 'No file selected for uploading'})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            filename_orig = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename_orig))
            orig_files.append(filename_orig)
    return orig_files


@app.route('/in_progress')
def index():
    title = 'В разработке'
    return render_template('progress.html', title=title)


# app name
@app.errorhandler(404)
# inbuilt function which takes error as parameter
def not_found(e):
    # defining function
    return render_template("404.html")


@app.route('/')
@login_required
@roles_accepted('admin')
def main():
    return render_template('main.html')
