# import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K
from keras.preprocessing import image


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


def load_image(img_path, size, show=False):
    img = image.load_img(img_path, target_size=size)
    img_tensor = image.img_to_array(img)  # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor,
                                axis=0)  # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.  # imshow expects values in the range [0, 1]
    if show:
        pass
        # plt.imshow(img_tensor[0])
        # plt.axis('off')
        # plt.show()
    return img_tensor


# TODO: реализовать метод для проверки модели
# def evulate_report(path, model, size, show=False):
#     pred_classes = []
#     true_classes = []
#     count = 0
#     for i in os.listdir(path):
#         for j in os.listdir(os.path.join(path, i)):
#             true_classes.append(int(i ) -1)
#             res = predict_class_scrap(os.path.join(path, i, j), model, size=size)
#             count += 1
#             print( 'Загружено %s' % count)
#             pred_classes.append(res[0])
#     print(classification_report(pred_classes, true_classes))

# TODO: Загрузка партии металлолома предсказывания для всей партии предсказания.

def predict_class_scrap(img, model, size=(512, 512), show=False, classes=False):
    img = load_image(img, size=size, show=show)
    if classes:
        predict = model.predict(img)
    else:
        predict = model.predict(img).argmax(axis=-1)
    K.clear_session()
    return predict


# TODO: размер картинки не высчитывается для предсказания
def predict_multiple_class_scrap(imgs, model, size=(512, 512), show=False, classes=True):
    images = []
    for im in imgs:
        img = load_image(im, size=size, show=show)
        images.append(img)
    images = np.vstack(images)
    if classes:
        predict = np.around(model.predict(images), decimals=2) * 100
    K.clear_session()
    average = np.average(predict, axis=0)
    return predict.tolist(), np.around(average, decimals=2).tolist(), predict.argmax(axis=-1).tolist()

# model = load_model(r'static/model_classification/ModelGlobal-12-0.87.hdf5', custom_objects={'f1': f1})
#     # print(predict_class_scrap(r'crop/43_5.png',model))
