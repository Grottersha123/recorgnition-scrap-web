import os

db_name = 'scrap'
db_user = 'test'

db_password = '6412121995q'

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

PATH_MODELS = os.path.join(ROOT_DIR, r'static/models_segmentation')
URL_DB = ''
TEST_FOLDER = r'static/TEST'

# DownloadTubes
# Для истории труб
TEST_FOLDER_UPLOAD_TUBE = r'/static/history_tube/UPLOAD_TUBE'

TEST_IMAGE_FOLDER_UPLOAD_TUBE = os.path.join(ROOT_DIR, r'static/history_tube/UPLOAD_TUBE')

TEST_FOLDER_MIX = r'static/TEST_MIX'
TEST_FOLDER_CLOG = r'static/TEST_CLOG'
TEST_FOLDER_SNOW = r'static/TEST_SNOW'
TEST_CLASS_SEG = r'static/TEST_CLASS_SEG'
TEST_FOLDER_TUBE = r'static/TEST_TUBES'

TEST_UPLOAD = r'static/TEST_UPLOAD'

TEST_SERVER_FOLDER = r'static/TEST_SERVER'

TEST_IMAGE_FOLDER = os.path.join(ROOT_DIR, TEST_FOLDER)
TEST_IMAGE_FOLDER_UPLOAD = os.path.join(ROOT_DIR, TEST_UPLOAD)
TEST_IMAGE_FOLDER_MIX = os.path.join(ROOT_DIR, TEST_FOLDER_MIX)
TEST_IMAGE_FOLDER_CLOG = os.path.join(ROOT_DIR, TEST_FOLDER_CLOG)
TEST_IMAGE_FOLDER_SNOW = os.path.join(ROOT_DIR, TEST_FOLDER_SNOW)
TEST_IMAGE_FOLDER_CLASS_SEG = os.path.join(ROOT_DIR, TEST_CLASS_SEG)
TEST_IMAGE_FOLDER_TUBE = os.path.join(ROOT_DIR, TEST_FOLDER_TUBE)
TEST_IMAGE_SERVER_FOLDER = os.path.join(ROOT_DIR, TEST_SERVER_FOLDER)
IMAGE_FOLDER = r'static/image'

UPLOAD_FOLDER = os.path.join(ROOT_DIR, IMAGE_FOLDER)
CLASS_MODEL = os.path.join(ROOT_DIR, r'static/model_classification/ModelGlobal-12-0.87.hdf5')
MODELS_SEGMENTATION = os.path.join(ROOT_DIR, r'static/models_segmentation/unet-256x256-ep7-num-489')
MODELS_SEGMENTATION_MIX = os.path.join(ROOT_DIR,
                                       r'static/models_segmentation/resnet50_segnet-dataset7-256x256-ep3-num-16')
MODELS_SEGMENTATION_CLOG = os.path.join(ROOT_DIR,
                                        r'static/models_segmentation/resnet50_unet-dataset9-512x512-ep2-num-73')
MODELS_SEGMENTATION_SNOW = os.path.join(ROOT_DIR,
                                        r'static/models_segmentation/resnet50_unet-dataset11-512x512-ep2-num-59')

MODELS_SEGMENTATION_CLASS = os.path.join(ROOT_DIR,
                                         r'static/models_segmentation/segnet-6_class_1024x1024')

MODELS_SEGMENTATION_TUBE = os.path.join(ROOT_DIR,
                                        r'static/models_segmentation/50_best_model.h5')

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
CLASSES = ["2АШ", "3А", "3АЖД", "5АЖД", "6А", "9А"]

test_image = [i for i in os.listdir(TEST_IMAGE_FOLDER) if 'out' not in i]

test_image_mix = [i for i in os.listdir(TEST_IMAGE_FOLDER_MIX) if 'mix' not in i and 'out' not in i]

test_image_clog = [i for i in os.listdir(TEST_IMAGE_FOLDER_CLOG) if 'clog' not in i and 'out' not in i]

test_image_snow = [i for i in os.listdir(TEST_IMAGE_FOLDER_SNOW) if 'snow' not in i and 'out' not in i]

test_image_seg_class = [i for i in os.listdir(TEST_IMAGE_FOLDER_CLASS_SEG) if 'circles' not in i and 'out' not in i]

test_image_tube = [i for i in os.listdir(TEST_IMAGE_FOLDER_TUBE) if 'circles' not in i and 'out' not in i]

test_upload_tube = [i for i in os.listdir(TEST_IMAGE_FOLDER_UPLOAD_TUBE) if 'circles' not in i and 'out' not in i]

conf_update = dict(
    # Пусть к меткам класса металлалома
    CLASS_MODEL=CLASS_MODEL,

    UPLOAD_FOLDER=UPLOAD_FOLDER,
    TEST_FOLDER_UPLOAD_TUBE=TEST_FOLDER_UPLOAD_TUBE,

    TEST_UPLOAD=TEST_UPLOAD,
    TEST_IMAGE_FOLDER=TEST_IMAGE_FOLDER,
    TEST_IMAGE_FOLDER_UPLOAD=TEST_IMAGE_FOLDER_UPLOAD,
    TEST_IMAGE_FOLDER_MIX=TEST_IMAGE_FOLDER_MIX,
    TEST_IMAGE_FOLDER_CLOG=TEST_IMAGE_FOLDER_CLOG,
    TEST_IMAGE_FOLDER_SNOW=TEST_IMAGE_FOLDER_SNOW,
    TEST_IMAGE_FOLDER_CLASS_SEG=TEST_IMAGE_FOLDER_CLASS_SEG,
    TEST_IMAGE_FOLDER_TUBE=TEST_IMAGE_FOLDER_TUBE,
    TEST_IMAGE_FOLDER_UPLOAD_TUBE=TEST_IMAGE_FOLDER_UPLOAD_TUBE,

    CLASSES=CLASSES,

    MODELS_SEGMENTATION=MODELS_SEGMENTATION,
    MODELS_SEGMENTATION_MIX=MODELS_SEGMENTATION_MIX,
    MODELS_SEGMENTATION_CLOG=MODELS_SEGMENTATION_CLOG,
    MODELS_SEGMENTATION_SNOW=MODELS_SEGMENTATION_SNOW,
    MODELS_SEGMENTATION_CLASS=MODELS_SEGMENTATION_CLASS,
    MODELS_SEGMENTATION_TUBE=MODELS_SEGMENTATION_TUBE
)


class Config(object):
    DEBUG = True
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{passsword}@localhost:5432/{name}'.format(user=db_user,
                                                                                             passsword=db_password,
                                                                                             name=db_name)
    SECURITY_MSG_INVALID_PASSWORD = ("Неправильно введен пароль", "error")
    SECURITY_MSG_PASSWORD_NOT_PROVIDED = ("Неправильно введен пароль", "error")
    SECURITY_MSG_USER_DOES_NOT_EXIST = ("Пользователь не существует", "error")
    FLASK_ADMIN_SWATCH = 'cerulean'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECURITY_PASSWORD_SALT = 'salt'
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECRET_KEY = '30419jskcnsosgriensdofjk3kdns34'
    USER_UNAUTHORIZED_ENDPOINT = '/logout'
    # SECRET_KEY = 'this-really-needs-to-be-changed'
    # SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']


class ProductionConfig(Config):
    DEBUG = False
    SECURITY_PASSWORD_SALT = 'salt'
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{passsword}@10.10.59.226:5432/{name}'.format(user=db_user,
                                                                                                passsword=db_password,
                                                                                                name=db_name)
    SECURITY_UNAUTHORIZED_VIEW = '/login'
    FLASK_ADMIN_SWATCH = 'cerulean'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    USER_UNAUTHORIZED_ENDPOINT = '/logout'
    SECRET_KEY = '30419jskcnsosgriensdofjk3kdns34'
    SECURITY_MSG_INVALID_PASSWORD = ("Неправильно введен пароль", "error")
    SECURITY_MSG_PASSWORD_NOT_PROVIDED = ("Неправильно введен пароль", "error")
    SECURITY_MSG_USER_DOES_NOT_EXIST = ("Пользователь не существует", "error")


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
