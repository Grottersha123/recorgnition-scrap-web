import json

import plotly


def generate_praphs(result, lables):
    font_val = dict(
        dict(family='Courier New, monospace', size=15, color='rgba(255, 255, 255, 0.5)')

    )
    graphs_av = [dict(
        data=[
            dict(
                x=lables,
                y=result[1],
                text=[str(round(i, 2)) + '%' for i in result[1]],
                textposition='auto',
                hoverinfo='text',
                font=dict(
                    color='white'
                ),
                showarrow=False,
                marker=dict(
                    color=['#FFB273', '#FFEC73', '#5CCCCC', '#8F6DD7', 'green', 'red'],
                ),
                type='bar'
            ),
        ],
        layout=dict(
            font=font_val,
            xaxis=dict(
                linewidth=2, linecolor='white'
            ),
            yaxis=dict(range=[0, 100], linewidth=2, linecolor='white'),
            plot_bgcolor='#373737',
            paper_bgcolor='#373737',
            title='Предсказание для всех слоев металлолома'
        )
    )]
    graphs = [dict(
        data=[
            dict(
                x=app.config['CLASSES'],
                y=i,
                text=[str(round(j, 2)) + '%' for j in i],
                textposition='auto',
                hoverinfo='text',
                font=dict(family='Arial', size=10,
                          color='rgba(245, 246, 249, 1)'),
                showarrow=False,
                marker=dict(
                    color=['#FFB273', '#FFEC73', '#5CCCCC', '#8F6DD7', 'green', 'red'],
                    line=dict(
                        width=1.5),
                ),
                type='bar'
            ),
        ],
        layout=dict(
            font=font_val,

            xaxis=dict(
                linewidth=2, linecolor='white'
            ),
            yaxis=dict(range=[0, 100], linewidth=2, linecolor='white'),
            paper_bgcolor='#373737',
            plot_bgcolor='#373737',
            title='Предсказание для одного слоя металлолома'
        )
    ) for i in result[0]]
    # print(Train.number)
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    graphJSONAverage = json.dumps(graphs_av, cls=plotly.utils.PlotlyJSONEncoder)
    return {'graphJSON': graphJSON, 'graphJSONAverage': graphJSONAverage}
