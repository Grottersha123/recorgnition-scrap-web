import secrets

from flask_security import UserMixin, RoleMixin

from app import db

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
                       )


class User(db.Model, UserMixin):
    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    password_a = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role',
                            secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    name = db.Column(db.String(255))
    surname = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    second_name = db.Column(db.String(255))
    token = db.Column(db.String(255))
    manufacture_name = db.Column(db.String(255))
    journal = db.relationship('Journal', backref='user', lazy=True)

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        # self.generate_token()

    def generate_token(self):
        self.token = secrets.token_urlsafe(20)

    def __repr__(self):
        return 'id - {} {}'.format(self.id, self.email)


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100), unique=True)
    description = db.Column(db.String(255))


def __repr__(self):
    return 'id - {} {}'.format(self.id, self.name)
