from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, DecimalField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, InputRequired
from wtforms.widgets.html5 import NumberInput

from scrap_app.models import rail_select, sender_select, number_train_select, date_select


class TrainForm(FlaskForm):
    number_train = IntegerField('Номер вагона', widget=NumberInput(),
                                validators=[DataRequired(message="Введите числовое значение")])
    sender = StringField('Поставщик', validators=[InputRequired()])
    rails = IntegerField('Номер рельс', widget=NumberInput(),
                         validators=[DataRequired(message="Введите числовое значение")])
    weight = DecimalField('Вес', widget=NumberInput(step=0.01),
                          validators=[DataRequired(message="Введите числовое значение")])
    submit_button = SubmitField('Подтвердить')


class TrainFormAnalysis(FlaskForm):
    number_train = QuerySelectField('Номер вагона', id='select_number', query_factory=number_train_select,
                                    allow_blank=False,
                                    validators=[InputRequired()], get_label='number')
    sender = QuerySelectField('Поставщик', id='select_sender', query_factory=sender_select,
                              allow_blank=False,
                              validators=[InputRequired()])
    number_rail = QuerySelectField('Номер рельс', id='select_rail', query_factory=rail_select,
                                   allow_blank=False,
                                   validators=[InputRequired()], get_label='number_rail')
    date = QuerySelectField('Дата', id='select_date',query_factory=date_select,
                            allow_blank=False,
                            validators=[InputRequired()], get_label='date')
    submit_button = SubmitField('Построить график')
