from sqlalchemy.dialects.postgresql import JSON

from app import db


class Train(db.Model):
    __tablename__ = 'car'

    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer)
    sender = db.Column(db.String)
    station = db.Column(db.String)
    date = db.Column(db.String)
    end = db.Column(db.DateTime)
    number_rail = db.Column(db.Integer)
    weight = db.Column(db.Float)
    scrap_layers = db.relationship('ScrapLayer', backref='scrap_layer', lazy=True)
    statistic = db.relationship('Statistic', backref='statistic', lazy=True)

    def __init__(self, **kwargs):
        super(Train, self).__init__(**kwargs)

    def __repr__(self):
        return '<id {}-{}-{}-{}>'.format(self.id, self.number, self.number_rail, self.scrap_layers)


class Statistic(db.Model):
    __tablename__ = 'statistic'
    id = db.Column(db.Integer, primary_key=True)
    count_layers = db.Column(db.Integer)
    pred_proba_layers = db.Column(JSON)
    car_id = db.Column(db.Integer(), db.ForeignKey('car.id'))
    date_add = db.Column(db.DateTime)

    def __init__(self, **kwargs):
        super(Statistic, self).__init__(**kwargs)

    def __repr__(self):
        return '<id {}-{}-{}-{}>'.format(self.id, self.count_layers, self.pred_proba_layers, self.car_id)


class ScrapLayer(db.Model):
    __tablename__ = 'scrap_layer'
    id = db.Column(db.Integer, primary_key=True)
    orig_pic = db.Column(db.String)
    mask_pic = db.Column(db.String)
    crop_pic = db.Column(db.String)
    pred_class = db.Column(db.Integer)
    pred_proba = db.Column(JSON)
    car_id = db.Column(db.Integer(), db.ForeignKey('car.id'))

    def __init__(self, **kwargs):
        super(ScrapLayer, self).__init__(**kwargs)

    def __repr__(self):
        return '<id {}-{}-{}>'.format(self.id, self.pred_class, self.car_id)


def rail_select():
    return Train.query


def sender_select():
    return Train.query


def number_train_select():
    return Train.query.distinct(Train.number)


def date_select():
    return Train.query
