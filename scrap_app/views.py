import datetime
import json
import logging
import os

import numpy as np
import requests
from bs4 import BeautifulSoup
from flask import render_template, jsonify, request, flash, Blueprint
from flask_cors import cross_origin
from flask_login import login_required
from flask_security import roles_accepted
from keras.engine.saving import load_model
from werkzeug.utils import secure_filename

from app import app, db, allowed_file
from classification import predict_multiple_class_scrap, f1
from config import TEST_FOLDER, TEST_FOLDER_MIX, TEST_FOLDER_CLOG, test_image, TEST_FOLDER_SNOW, test_image_mix, \
    test_image_clog, test_image_snow, TEST_UPLOAD, ROOT_DIR, TEST_IMAGE_SERVER_FOLDER
from scrap_app.forms import TrainForm, TrainFormAnalysis
from scrap_app.models import Train, Statistic, ScrapLayer
from segmentation import get_multiple_segmentation, get_multiple_segmentation_mix, get_multiple_segmentation_class

test_image_checkbox = [os.path.join(TEST_FOLDER, i) for i in test_image]

test_image_mix = [os.path.join(TEST_FOLDER_MIX, i) for i in test_image_mix]
test_image_clog = [os.path.join(TEST_FOLDER_CLOG, i) for i in test_image_clog]
test_image_snow = [os.path.join(TEST_FOLDER_SNOW, i) for i in test_image_snow]

# CONFIG
scrap = Blueprint('scrap', __name__, template_folder='templates')


@scrap.route('/scrap_main')
def home():
    # title = 'Система классификации покупного лома с помощью промышленного фото анализа и машинного обучения'
    title = 'Система классификации покупного лома'
    return render_template('scrap_template/scrap.html', title=title)


# АPI ДЛЯ МЕТАЛЛОЛОМА
@scrap.route('/_get_sender/')
def _get_senders():
    number = request.args.get('number', '01', type=int)
    res = Train.query.filter_by(number=number)
    senders = [(row.id, row.sender) for row in res.distinct(Train.sender).all()]
    rails = [(row.id, row.number_rail) for row in res.distinct(Train.number_rail).all()]
    dates = [(row.id, row.date) for row in res.distinct(Train.date).all()]
    return jsonify([senders, rails, dates])


@scrap.route('/post_info_scrap', methods=["POST"])
def post_info_scrap():
    if request.method == 'POST':
        number = request.form.get('number', type=int)
        sender = request.form.get('sender', type=str)
        number_rail = request.form.get('number_rail', type=int)
        weight = request.form.get('weight', type=int)
        train = Train(
            number=number,
            sender=sender,
            date=datetime.datetime.now().date(),
            number_rail=number_rail,
            weight=weight)
        db.session.add(train)
        db.session.commit()
        return jsonify(id=train.id)


headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Max-Age': '3600',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
}
import urllib.request


def create_dir(path_1, path_2):
    import os
    path_1_full = os.path.join(TEST_IMAGE_SERVER_FOLDER, path_1)
    path_2_full = os.path.join(TEST_IMAGE_SERVER_FOLDER, path_1, path_2)
    if not os.path.exists(path_1_full):
        os.makedirs(path_1_full)
    if not os.path.exists(path_2_full):
        os.makedirs(path_2_full)
    return path_2_full


def download_create_img(url, path):
    req = requests.get(url, headers)
    soup = BeautifulSoup(req.content, 'html.parser')
    res = soup.find_all("a")
    for r in res:
        href_name = r.get('href')
        if ('jpg' or 'png') in href_name:
            href = '{}/{}'.format(url, href_name)
            urllib.request.urlretrieve(href, os.path.join(path, href_name))
            logging.info(href + ' ' + path)


@scrap.route('/post_json_scrap', methods=["POST"])
@cross_origin()
def post_json_scrap():
    if request.method == 'POST':
        url = request.form.get('url', type=str)
        path_split = os.path.split(url)
        path_2 = path_split[-1]
        path_1 = os.path.split(path_split[0])[-1]
        path = create_dir(path_1, path_2)
        logging.info('Create dirs')
        download_create_img(url, path)
        logging.info('dowload_img')
        train_id = int(path_2)
        chose_image = [os.path.split(i)[-1] for i in os.listdir(path) if 'out' not in i and 'crop' not in i]

        batch_images = get_multiple_segmentation(chose_image, os.path.join(path),
                                                 app.config['MODELS_SEGMENTATION'], is_test=False, test_folder=path)
        crop_files = [os.path.join(ROOT_DIR, i[-1]) for i in batch_images]
        model = load_model(app.config['CLASS_MODEL'], custom_objects={'f1': f1})
        result = predict_multiple_class_scrap(crop_files, model)
        # Нужно найти id c train в Statistic и обновить таблицу
        # train = Train.query.filter_by(train_id=train_id).first()
        # print(train)
        statistic = Statistic(car_id=train_id,
                              date_add=datetime.datetime.now(),
                              count_layers=len(chose_image),
                              pred_proba_layers=[float(i) for i in result[1]])
        db.session.add(statistic)
        for index, im in enumerate(batch_images):
            scrap_layer = ScrapLayer.query.filter_by(orig_pic=im[0]).first()
            if scrap_layer:
                scrap_layer.mask_pic = im[1]
                scrap_layer.crop_pic = [2]
                scrap_layer.pred_class = int(np.argmax(result[0][index])),
                scrap_layer.pred_proba = [float(i) for i in result[0][index]],
                scrap_layer.car_id = train_id

            else:
                scrap_layer = ScrapLayer(orig_pic=im[0],
                                         mask_pic=im[1],
                                         crop_pic=[2],
                                         pred_class=int(np.argmax(result[0][index])),
                                         pred_proba=[float(i) for i in result[0][index]],
                                         car_id=train_id)
            db.session.add(scrap_layer)
        db.session.commit()
        return jsonify(id=statistic.id)


@scrap.route('/post_class_scrap', methods=["POST"])
def get_class_scrap():
    if request.method == 'POST':
        file = request.files['file']
        if file.filename == '':
            resp = jsonify({'message': 'No file selected for uploading'})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path_file = os.path.join(app.config['TEST_UPLOAD'], filename)
            file.save(os.path.join(app.config['TEST_IMAGE_FOLDER_UPLOAD'], filename))
            print(os.path.join(app.config['TEST_IMAGE_FOLDER_UPLOAD'], filename))
            chose_image = [filename]
            batch_images = get_multiple_segmentation(chose_image, app.config['TEST_IMAGE_FOLDER_UPLOAD'],
                                                     app.config['MODELS_SEGMENTATION'], is_test=False,
                                                     test_folder=app.config['TEST_UPLOAD'])
            crop_files = [os.path.join(ROOT_DIR, i[-1]) for i in batch_images]
            model = load_model(app.config['CLASS_MODEL'], custom_objects={'f1': f1})
            # TODO: Добавить в базу данных предсказанья
            # TODO: Добавить обработку если есть фотография уже в static, то не добавлять в бд.

            result = predict_multiple_class_scrap(crop_files, model)

            graphs_av_3d = [dict(class_pred=cls, prediction=int(res)) for cls, res in
                            zip(app.config['CLASSES'], result[1])]
            return jsonify(result=graphs_av_3d, class_pred=app.config['CLASSES'][result[2][0]])


@scrap.route('/post_segment_clog', methods=["GET", "POST"])
def get_segment_clog():
    if request.method == 'POST':
        option = request.form.get('option', type=int)
        file = request.files['file']
        if file.filename == '':
            resp = jsonify({'message': 'No file selected for uploading'})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path_file = os.path.join(app.config['TEST_UPLOAD'], filename)
            file.save(os.path.join(app.config['TEST_IMAGE_FOLDER'], filename))
            chose_image = [filename]
            options_model = [app.config['MODELS_SEGMENTATION_MIX'], app.config['MODELS_SEGMENTATION_CLOG'],
                             app.config['MODELS_SEGMENTATION_SNOW']]
            options_dir = [app.config['TEST_IMAGE_FOLDER_UPLOAD'], app.config['TEST_IMAGE_FOLDER_UPLOAD'],
                           app.config['TEST_IMAGE_FOLDER_UPLOAD']]
            option_static = [TEST_UPLOAD, TEST_UPLOAD, TEST_UPLOAD]
            out, batch_images = get_multiple_segmentation_mix(chose_image, options_dir[option - 1],
                                                              options_model[option - 1], is_test=True,
                                                              test_folder=option_static[option - 1])
        graphs_3d = [[dict(country=int(cls), visits=int(res)) for cls, res in zip(*i)] for i in
                     out]
        mean = list(map(lambda x: sum(x) / len(x), zip(*list(zip(*out))[1])))
        # [dict(country=cls, visits=int(res)) for cls, res in zip(app.config['CLASSES'], result[1])]
        graphs_average_3d = [dict(class_pred=cls + 1, pred=int(res)) for cls, res in enumerate(mean)]

        return jsonify(graphs_average_3d)


# ОКОНЧАНИЕ API

@scrap.route('/segmentation_image', methods=["GET", "POST"])
@login_required
@roles_accepted('admin', 'scrap')
def segmentation_image():
    title = 'Сегментация металлолома'
    if request.method == 'POST':
        batch_images = request.form.getlist('image[]')
        chose_image = [os.path.split(i)[-1] for i in batch_images]

        if request.form['submit_button'] == 'sub_seg':
            batch_images = get_multiple_segmentation(chose_image, app.config['TEST_IMAGE_FOLDER'],
                                                     app.config['MODELS_SEGMENTATION'], is_test=True)
            return render_template('scrap_template/segmentation.html', title=title, images_test=test_image_checkbox,
                                   images=batch_images)
        if request.form['submit_button'] == 'sub_class':
            batch_images = get_multiple_segmentation(chose_image, app.config['TEST_IMAGE_FOLDER'],
                                                     app.config['MODELS_SEGMENTATION'], is_test=True, cnts=False)
            crop_files = [os.path.split(i[-1])[-1] for i in batch_images]
            out_pred, out_pred_average, batch_img = get_multiple_segmentation_class(crop_files,
                                                                                    app.config['TEST_IMAGE_FOLDER'],
                                                                                    app.config[
                                                                                        'MODELS_SEGMENTATION_CLASS'],
                                                                                    is_test=True)

            graphs_av_3d = [dict(country=cls, visits=int(res)) for cls, res in
                            zip(app.config['CLASSES'], out_pred_average)]

            print(out_pred, out_pred_average)

            graphs_3d = [[dict(country=app.config['CLASSES'][cls - 1], visits=int(res)) for cls, res in zip(num, per)]
                         for num, per in
                         out_pred]
            print(graphs_3d)
            # graphJSONAverage3d=json.dumps(graphs_av_3d),
            return render_template('scrap_template/segmentation_list.html', title=title,
                                   images=batch_img,
                                   graphJSON3d=json.dumps(graphs_3d), graphJSONAverage3d=json.dumps(graphs_av_3d))


    if request.method == 'GET':
        return render_template('scrap_template/segmentation.html', title=title, images_test=test_image_checkbox)


# зассоренность
@scrap.route('/clog_image', methods=["GET", "POST"])
@login_required
@roles_accepted('admin', 'scrap')
def segment_clog():
    title = 'Распознавание засора в металлоломе'
    if request.method == 'POST':
        if request.form['submit_button'] == "Смешенность металлалома":
            flash('Данные по вагону добавлены. \n Выберите фотографии')
            return render_template('scrap_template/clog_upload.html', title=title, option=1, test_image_checkbox=test_image_mix)
        if request.form['submit_button'] == "Распознавание стружки и грязи":
            flash('Данные по вагону добавлены. \n Выберите фотографии')
            return render_template('scrap_template/clog_upload.html', title=title, option=2,
                                   test_image_checkbox=test_image_clog)
        if request.form['submit_button'] == "Распознавание снега":
            flash('Данные по вагону добавлены. \n Выберите фотографии')
            return render_template('scrap_template/clog_upload.html', title=title, option=3,
                                   test_image_checkbox=test_image_snow)
        if request.form['submit_button'] == "Распознавание":
            option = int(request.form.get('options'))
            options_model = [app.config['MODELS_SEGMENTATION_MIX'], app.config['MODELS_SEGMENTATION_CLOG'],
                             app.config['MODELS_SEGMENTATION_SNOW']]
            options_dir = [app.config['TEST_IMAGE_FOLDER_MIX'], app.config['TEST_IMAGE_FOLDER_CLOG'],
                           app.config['TEST_IMAGE_FOLDER_SNOW']]
            option_static = [TEST_FOLDER_MIX, TEST_FOLDER_CLOG, TEST_FOLDER_SNOW]
            batch_images = request.form.getlist('image[]')
            chose_image = [os.path.split(i)[-1] for i in batch_images]
            out, batch_images = get_multiple_segmentation_mix(chose_image, options_dir[option - 1],
                                                              options_model[option - 1], is_test=True,
                                                              test_folder=option_static[option - 1])
            print(out, batch_images)
            graphs_3d = [[dict(country=int(cls), visits=int(res)) for cls, res in zip(*i)] for i in
                         out]
            print(list(zip(*out))[1])
            mean = list(map(lambda x: sum(x) / len(x), zip(*list(zip(*out))[1])))
            # [dict(country=cls, visits=int(res)) for cls, res in zip(app.config['CLASSES'], result[1])]
            graphs_average_3d = [dict(country=cls + 1, visits=int(res)) for cls, res in enumerate(mean)]

            return render_template('scrap_template/clog_list.html', title=title, images=batch_images,
                                   graphJSONAverage3d=json.dumps(graphs_average_3d),
                                   graphJSON3d=json.dumps(graphs_3d))
    flash('Выберите нужную опцию для распознавания')
    return render_template('scrap_template/clog_upload.html', title=title)


@scrap.route('/classification_image', methods=["GET", "POST"])
@login_required
@roles_accepted('admin', 'scrap')
def classification_image():
    title = 'Классификация металлолома'
    form = TrainForm()
    if form.validate_on_submit():
        flash('Данные по вагону добавлены. \n Выберите фотографии')
        train = Train(
            number=form.number_train.data,
            sender=form.sender.data,
            number_rail=form.rails.data,
            date=datetime.datetime.now().date(),
            weight=form.weight.data)
        db.session.add(train)
        db.session.commit()
        return render_template('scrap_template/classification_upload.html', title=title,
                               images_test=test_image_checkbox,
                               train_id=train.id)
    if request.method == 'POST':
        batch_images = request.form.getlist('image[]')
        train_id = request.form.get('train_id')

        chose_image = [os.path.split(i)[-1] for i in batch_images]

        batch_images = get_multiple_segmentation(chose_image, app.config['TEST_IMAGE_FOLDER'],
                                                 app.config['MODELS_SEGMENTATION'], is_test=True)
        crop_files = [os.path.join(ROOT_DIR, i[-1]) for i in batch_images]
        model = load_model(app.config['CLASS_MODEL'], custom_objects={'f1': f1})

        result = predict_multiple_class_scrap(crop_files, model)
        train = Train.query.get(train_id)
        print(train)
        statistic = Statistic(car_id=train_id,
                              date_add=datetime.datetime.now(),
                              count_layers=len(chose_image),
                              pred_proba_layers=[float(i) for i in result[1]])
        db.session.add(statistic)
        for index, im in enumerate(batch_images):
            scrap_layer = ScrapLayer(orig_pic=im[0],
                                     mask_pic=im[1],
                                     crop_pic=[2],
                                     pred_class=int(np.argmax(result[0][index])),
                                     pred_proba=[float(i) for i in result[0][index]],
                                     car_id=train_id)
            db.session.add(scrap_layer)
        train.end = datetime.datetime.now()
        db.session.commit()
        graphs_av_3d = [dict(country=cls, visits=int(res)) for cls, res in zip(app.config['CLASSES'], result[1])]
        print(graphs_av_3d)
        graphs_3d = [[dict(country=cls, visits=int(res)) for cls, res in zip(app.config['CLASSES'], i)] for i in
                     result[0]]
        print(graphs_3d)
        resp = jsonify({'message': result})
        resp.status_code = 201
        # print(files)
        return render_template('scrap_template/classification_list.html', title=title, images=batch_images,
                               result=result,
                               graphJSONAverage3d=json.dumps(graphs_av_3d),
                               graphJSON3d=json.dumps(graphs_3d))

    if request.method == "GET":
        # print(test_image_checkbox)
        return render_template('scrap_template/submit_classification.html', title=title, form=form)
        # return render_template('classification_upload.html', title=title, images_test=test_image_checkbox)


def select_scrap(number_train, sender, number_rail, date):
    result = db.session.execute(
        'SELECT pred_proba_layers from train join statistic s on train.id = s.car_id where '
        'sender= :sender '
        'and number= :number_train '
        'and number_rail= :number_rail '
        'and date= :date '
        'order by train.id  desc limit 1',
        {'number_train': number_train, 'sender': sender, 'date': date,
         'number_rail': number_rail})

    return result


# SELECT pred_proba_layers,  from train join statistic s on train.id = s.train_id where sender='ЧТПЗ' order by train.id  desc limit 1
@scrap.route('/dash_scrap', methods=["GET", "POST"])
def dash_board():
    title = 'Аналитический отчет'
    form = TrainFormAnalysis()
    if request.method == 'POST':
        print(form.sender.raw_data)
        number_train = form.number_train.data.number
        sender = form.sender.data.sender
        number_rail = form.number_rail.data.number_rail
        print(form.sender.raw_data)
        date = form.date.data.date
        result = select_scrap(number_train, sender, number_rail, date)
        results = [i for i in result]
        results = [[results]] if not results else results
        print(results)
        graphs_av_3d = [dict(country=cls, visits=int(res)) for cls, res in zip(app.config['CLASSES'], results[0][0])]
        return render_template('scrap_template/scrap_analytic.html', form=form, title=title,
                               graphJSONAverage3d=json.dumps(graphs_av_3d))
    return render_template('scrap_template/scrap_analytic.html', form=form, title=title)
