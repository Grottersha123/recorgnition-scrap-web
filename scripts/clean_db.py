import shutil

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# CONNECTING TO DATABASE
from config import db_name, db_user, db_password

try:
    conn = psycopg2.connect(dbname='postgres',
                            user='postgres',
                            host='localhost',
                            password='1234')
except:
    print("I am unable to connect to the database.")
# EXECUTE QUERYSET
queryset_list = [
    "drop database %s  ;" % db_name,
    "drop user %s  ;" % db_user,
    "create user %s createdb password '%s';" % (db_user, db_password),
    "create database %s with owner='%s' encoding='utf-8' lc_collate='C' lc_ctype='C' template template0;" % (
    db_name, db_user)
]
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur = conn.cursor()
for query in queryset_list:
    try:
        print('Выполен запрос %s' % query)
        cur.execute(query)
    except Exception as error:
        print(error)
print('\n')

print('удаляем миграции')
try:
    shutil.rmtree('..\migrations')
    # shutil.rmtree('..\app\')
except:
    print('no delete')
