import os
import time
from typing import List

import cv2
import numpy as np
from keras import backend as k
from keras_segmentation.predict import predict, model_from_checkpoint_path

from config import IMAGE_FOLDER, TEST_FOLDER
from utils import create_new_img


def average_predict(out_pred):
    result = []
    for out, pred in out_pred:
        temp = pred[:]
        for cls in range(1, 7):
            if cls not in out:
                temp.insert(cls - 1, 0)
        result.append(temp)
    mean_result = np.average(np.array(result), axis=0)
    return np.around(mean_result, decimals=2).tolist()


def crop_by_mask(img, out, name, cnts=True, exclude_mask=True):
    print(img, out, name)
    image = cv2.imread(img)
    out = cv2.imread(out)
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    out = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)
    if exclude_mask:
        _, out2 = cv2.threshold(out, 137, 255, cv2.THRESH_BINARY_INV)
    else:
        _, out2 = cv2.threshold(out, 137, 255, cv2.THRESH_BINARY)

    if cnts:
        b, g, r = cv2.split(image)
        rgba = [b, g, r, out2]
        dst = cv2.merge(rgba, 4)
    else:
        src1_mask = cv2.cvtColor(out2, cv2.COLOR_GRAY2BGR)  # change mask to a 3 channel image
        mask_out = cv2.subtract(src1_mask, image)
        dst = cv2.subtract(src1_mask, mask_out)
    cv2.imwrite(name, dst)
    return dst





def add_transparent_mask(src1, src2, name, one_new=(121, 70, 203)[::-1], sec_new=(255, 207, 36)[::-1],
                         lbls=['1 class', '2 class']):
    src1 = cv2.imread(src1)
    height1, width1, channel1 = src1.shape
    src2 = cv2.imread(src2)

    src2 = cv2.resize(src2, (512, 512))

    height, width, channels = src2.shape

    one = (183, 244, 155)
    sec = (207, 248, 132)
    # channels_xy = [132, 248, 207]
    for x in range(0, width):
        for y in range(0, height):
            if any(src2[y, x] == one):
                src2[y, x] = one_new
            if any(src2[y, x] == sec):
                src2[y, x] = sec_new
            if all(src2[y, x] != sec_new) and all(src2[y, x] != one_new):
                src2[y, x] = (0, 0, 0)
    src2 = cv2.resize(src2, (width1, height1))
    dst = cv2.addWeighted(src1, 0.6, src2, 0.4, 10)
    color = [sec_new, one_new]
    offset = 100
    x, y = 50, 60
    for idx, lbl in enumerate(lbls):
        cv2.putText(dst, lbl, (x, y + offset * idx), cv2.FONT_HERSHEY_SIMPLEX, 2, color[idx], 5)
    cv2.imwrite(name, dst)
    return dst


def calculate_average_segment(out_return):
    """
    :type out_return: numpy prediction
    """
    flatten = out_return.ravel()
    res = np.unique(flatten, return_counts=True)
    not_class = int((res[1][0] / flatten.shape[0]) * 100 / (len(res[0]) - 1))
    mean_result: List[int] = list(map(lambda x: round((x / flatten.shape[0]) * 100) + not_class, res[1]))
    return res[0][1:], mean_result[1:]


def get_segmentation_mix(inp_path, out_path, checkpoint_path, model=None, is_transparent=True):
    if not model is None:
        print(out_path)
        out_return = predict(model=model, inp=inp_path, out_fname=out_path)
    else:
        print(out_path)
        out_return = predict(model=model, inp=inp_path, out_fname=out_path, checkpoints_path=checkpoint_path)
    if is_transparent:
        dst = add_transparent_mask(inp_path, out_path, create_new_img(out_path, 'mix'))
    return calculate_average_segment(out_return), create_new_img(out_path, 'mix')


def get_multiple_segmentation_mix(inps_path_list, out_path, checkpoint_path, model=None,
                                  is_test=False, test_folder=''):
    if model is None and (checkpoint_path is not None):
        model = model_from_checkpoint_path(checkpoint_path)
    batch_segmentation = []
    out_predict = []

    for img in inps_path_list:
        filename_seg = 'out_' + img
        path_orig, path_seg = (
            os.path.join(out_path, img),
            os.path.join(out_path, filename_seg)
        )
        start = time.time()

        out, image_seg_color = get_segmentation_mix(path_orig, path_seg, checkpoint_path,
                                                    model=model, )
        if is_test:
            batch_segmentation.append(list(map(lambda x: os.path.join(test_folder, os.path.split(x)[-1]),
                                               [path_orig, image_seg_color])))
            out_predict.append(out)
        if not is_test:
            batch_segmentation.append(list(map(lambda x: os.path.join(IMAGE_FOLDER, os.path.split(x)[-1]),
                                               [path_orig, image_seg_color])))
            out_predict.append(out)
        print(time.time() - start)

    k.clear_session()
    return out_predict, batch_segmentation


# добавить сегментацию, и добавить
# нужный вывод строк на выход вырезанная фотография

def get_multiple_segmentation_class(inps_path_list, out_path, checkpoint_path, model=None,
                                    is_test=False, test_folder=''):
    if model is None and (checkpoint_path is not None):
        model = model_from_checkpoint_path(checkpoint_path)
    batch_segmentation = []
    out_predict = []

    for img in inps_path_list:
        print(img)
        filename_seg = 'out_class_' + img
        path_orig, path_seg = (
            os.path.join(out_path, img),
            os.path.join(out_path, filename_seg)
        )
        start = time.time()

        out, image_seg_color = get_segmentation_mix(path_orig, path_seg, checkpoint_path,
                                                    model=model, is_transparent=False)
        if is_test:
            batch_segmentation.append(list(map(lambda x: os.path.join(TEST_FOLDER, os.path.split(x)[-1]),
                                               [path_orig, filename_seg])))
            out_predict.append(out)
        if not is_test:
            batch_segmentation.append(list(map(lambda x: os.path.join(test_folder, os.path.split(x)[-1]),
                                               [path_orig, filename_seg])))
            out_predict.append(out)
        print(time.time() - start)

    k.clear_session()
    return out_predict, average_predict(out_predict), batch_segmentation


# TODO:// разобраться в чем дело с checkpoint_path
def get_segmentation(inp_path, out_path, checkpoint_path=None, color=(23, 185, 247), model=None, cnts=True):
    print(inp_path, out_path, )
    if not model is None:
        out_return = predict(model=model, inp=inp_path, out_fname=out_path)
    else:
        out_return = predict(model=model, inp=inp_path, out_fname=out_path, checkpoints_path=checkpoint_path)

    out = cv2.imread(out_path)
    img = np.copy(out)
    out = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)
    _, out2 = cv2.threshold(out, 137, 255, cv2.THRESH_BINARY)

    img[out2 == 0] = color
    img[out2 == 255] = [38, 38, 38]
    cv2.imwrite(create_new_img(out_path, 'color'), img)
    if cnts:
        path_crop = create_new_img(out_path, 'crop')
    else:
        path_crop = create_new_img(out_path, 'crop_b')
    crop_by_mask(inp_path, out_path, path_crop, cnts=cnts)
    # os.remove(out_path)
    return out_return, create_new_img(out_path, 'color'), path_crop


# TODO:// не выводит предсказание о том какие пиксели надо сегментировать
def get_multiple_segmentation(inps_path_list, out_path, checkpoint_path, color=(23, 185, 247), model=None,
                              is_test=False, test_folder=None, cnts=True):
    if model is None and (checkpoint_path is not None):
        model = model_from_checkpoint_path(checkpoint_path)
    batch_segmentation = []
    start = time.time()
    for img in inps_path_list:
        filename_seg = 'out_' + img
        path_orig, path_seg = (
            os.path.join(out_path, img),
            os.path.join(out_path, filename_seg)
        )
        out, image_seg_color, image_crop = get_segmentation(path_orig, path_seg, checkpoint_path, color=color,
                                                            model=model, cnts=cnts)
        if is_test:
            batch_segmentation.append(list(map(lambda x: os.path.join(TEST_FOLDER, os.path.split(x)[-1]),
                                               [path_orig, image_seg_color, image_crop])))
        if not is_test:
            batch_segmentation.append(list(map(lambda x: os.path.join(test_folder, os.path.split(x)[-1]),
                                               [path_orig, image_seg_color, image_crop])))
        print(time.time() - start)

    k.clear_session()
    return batch_segmentation

    # for i in os.listdir(r'pred'):
    #   crop_by_mask(os.path.join(r'D:\Git_project\wer_rec_scrap\orig', i), os.path.join(r'D:\Git_project\wer_rec_scrap\pred',i), i)
    # get_segmentation(r'D:\Git_project\wer_rec_scrap\orig\27_4.png', r'static/models_segmentation/unet-256x256-ep7-num-489')
