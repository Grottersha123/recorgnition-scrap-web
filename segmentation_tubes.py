import math
import os

import cv2
import keras
import numpy as np
import segmentation_models as sm
from keras import backend as k

# classes for data loading and preprocessing
from config import TEST_FOLDER_TUBE


class Dataset:
    """CamVid Dataset. Read images, apply augmentation and preprocessing transformations.

    Args:
        images_dir (str): path to images folder
        masks_dir (str): path to segmentation masks folder
        class_values (list): values of classes to extract from segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing
            (e.g. noralization, shape manipulation, etc.)

    """

    CLASSES = ['background', 'tube']

    def __init__(
            self,
            images,
            images_dir,
            classes=None,
            preprocessing=None,
            size=(320, 480)
    ):
        self.ids = images
        self.images_fps = [os.path.join(images_dir, image_id) for image_id in self.ids]
        print(self.images_fps)

        # convert str names to class values on masks
        # self.class_values = [self.CLASSES.index(cls.lower()) for cls in classes]

        self.preprocessing = preprocessing
        self.size = size

    def __getitem__(self, i):

        # read data
        image = cv2.imread(self.images_fps[i])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        if self.size:
            image = cv2.resize(image, self.size)
        if self.preprocessing:
            image = self.preprocessing(image)

        return image, self.images_fps[i]

    def __len__(self):
        return len(self.ids)


class Dataloder(keras.utils.Sequence):
    """Load data from dataset and form batches

    Args:
        dataset: instance of Dataset class for image loading and preprocessing.
        batch_size: Integet number of images in batch.
        shuffle: Boolean, if `True` shuffle image indexes each epoch.
    """

    def __init__(self, dataset, batch_size=1, shuffle=False):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.indexes = np.arange(len(dataset))

        self.on_epoch_end()

    def __getitem__(self, i):
        # collect batch data
        start = i * self.batch_size
        stop = (i + 1) * self.batch_size
        data = []
        for j in range(start, stop):
            data.append(self.dataset[j])

        # transpose list of lists
        batch = [np.stack(samples, axis=0) for samples in zip(*data)]

        return batch

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.indexes) // self.batch_size


def find_anomalies(random_data, coeff=None):
    # Set upper and lower limit to 3 standard deviation
    # Среднеквадратическое отклонение
    anomalies = []
    random_data_std = np.std(random_data)
    random_data_mean = np.mean(random_data)
    anomaly_cut_off = random_data_std * coeff

    lower_limit = random_data_mean - anomaly_cut_off
    upper_limit = random_data_mean + anomaly_cut_off
    # Generate outliers
    for outlier in random_data:
        if outlier > upper_limit or outlier < lower_limit:
            anomalies.append(outlier)
    return anomalies

def apply_anomalies(random_data, coeff=None):
    data = [d['radius'] for d in random_data]
    data.sort()
    anomal = find_anomalies(data, coeff=coeff)
    random_data = [rd for rd in random_data if rd['radius'] not in anomal]
    return random_data

def find_circles(image, model, size=None, coeff=2.7):
    image = np.expand_dims(image, axis=0)
    pr_mask = model.predict(image).round()
    pr_mask_normallize = cv2.cvtColor(pr_mask[..., 0].squeeze(), cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(pr_mask_normallize, cv2.COLOR_RGB2GRAY)
    if size:
        gray = cv2.resize(gray, size)
    ret, thresh = cv2.threshold(gray, 0, 255, 0)

    image_convertScaleAbs = cv2.convertScaleAbs(thresh)
    countours, hierarchy = cv2.findContours(image_convertScaleAbs, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    coordinates = []
    for ind, i in enumerate(countours):
        M = cv2.moments(i)
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
        else:
            # set values as what you need in the situation
            cX, cY = 0, 0
        x, y = i[0][0]
        radius = int(math.sqrt((x - cX) ** 2 + (y - cY) ** 2) * 2.5)
        coordinates.append(dict(radius=radius, center=(cX, cY)))
    coordinates = apply_anomalies(coordinates, coeff=coeff)
    return coordinates, thresh


def model_compile(path_model):
    BACKBONE = 'mobilenet'
    CLASSES = ['tube']
    preprocess_input = sm.get_preprocessing(BACKBONE)
    LR = 0.0001
    # define network parameters
    n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
    activation = 'sigmoid' if n_classes == 1 else 'softmax'

    # create model
    print(BACKBONE, n_classes, activation)
    model = sm.Unet(BACKBONE, classes=n_classes, activation=activation)
    # define optomizer
    optim = keras.optimizers.Adam(LR)

    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    # set class weights for dice_loss (car: 1.; pedestrian: 2.; background: 0.5;)
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.BinaryFocalLoss() if n_classes == 1 else sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)

    # actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
    # total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss

    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]

    # compile keras model with defined optimozer, loss and metrics
    model.compile(optim, total_loss, metrics)
    model.load_weights(path_model)
    return model, preprocess_input


# TODO:// сохранять маску и выводить ее на фронт

def load_data(images, images_dir, path_model):
    # ["sky","2аш", "3а", "3ажд", "5жд", "6а", "9а"]
    model, preprocess_input = model_compile(path_model)
    test_dataset = Dataset(
        images,
        images_dir,
        # augmentation=get_validation_augmentation(),
        size=(1024, 768),
        preprocessing=preprocess_input,
    )

    test_dataloader = Dataloder(test_dataset, batch_size=1, shuffle=False)
    result = []
    for sample in test_dataset:
        image, name = sample
        coordinates, tresh = find_circles(image, model)
        name_out = name.split('.')[0] + '_out' + '.png'
        cv2.imwrite(name_out, tresh)
        result.append((coordinates, name, name_out))
    k.clear_session()
    return result


def draw_circle(image, center, radius, index):
    cv2.circle(image, center, int(radius), (0, 0, 255), 1, 1)
    cv2.putText(image, str(index), center, 1, 1, (255, 255, 0))
    return image


def draw_circles_images(images_coordinates):
    images_path = []
    for sample in images_coordinates:
        coordinates, name_image, name_out = sample
        image = cv2.imread(name_image)
        for index, coor in enumerate(coordinates):
            textsize = cv2.getTextSize(str(index + 1), 1, 1, 2)[0]

            # get coords based on boundary
            textX = int(coor['center'][0] - (textsize[0] / 2))
            textY = int(coor['center'][1] + (textsize[1] / 2))
            cv2.circle(image, coor['center'], int(coor['radius']), (0, 0, 255), 1, 1)
            cv2.putText(image, str(index + 1), (textX, textY), 1, 1, (255, 255, 0))

        name_circles = name_image.split('.')[0] + '_circles' + '.png'
        cv2.imwrite(name_circles, image)
        images_path.append(list(map(lambda x: os.path.join(TEST_FOLDER_TUBE, os.path.split(x)[-1]),
                                    [name_image, name_out, name_circles])))
    return images_path


# Придумать как высчитывать общее количество


hsv_min = np.array((16, 157, 239), np.uint8)
hsv_max = np.array((255, 255, 255), np.uint8)


# size (1024, 768)

def include_area(img, size=None):
    # 255, 207, 36
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)  # меняем цветовую модель с BGR на HSV
    thresh = cv2.inRange(hsv, hsv_min, hsv_max)  # применяем цветовой фильтр
    if size:
        thresh = cv2.resize(thresh, size)
    cv2.imwrite('test.png', thresh)
    return thresh


def crop_by_mask_black(image, out, name=None, exclude_mask=False, cnts=False):
    # print(img, out, name)
    if isinstance(image, str):
        image = cv2.imread(image)
    if isinstance(out, str):
        out = cv2.imread(out)
    if exclude_mask:
        _, out = cv2.threshold(out, 137, 255, cv2.THRESH_BINARY_INV)
    print(image.shape, out.shape)
    dst = cv2.bitwise_and(image, out)
    if name:
        cv2.imwrite(name, dst)
    return dst
