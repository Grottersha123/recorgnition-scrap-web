function create_plot_pred(id_graph, data_m,
                          title_main, title_axis, value_title, label, max_x = 100) {
    var colors = [
        am4core.color("#FFCF24"),
        am4core.color("#7946CB"),
        am4core.color("#FF6F91"),
        am4core.color("#FF9671"),
        am4core.color("#FFC75F"),
        am4core.color("#F9F871")
    ];
    am4core.useTheme(am4themes_dark);
    am4core.useTheme(am4themes_animated);
    am4core.ready(function () {
        var chart = am4core.create(id_graph, am4charts.XYChart3D);
        chart.colors.list = colors;

// Add data
        chart.data = data_m;
        let title = chart.titles.create();
        title.text = title_main;
        title.fontSize = 25;
        title.marginBottom = 30;

// Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.renderer.labels.template.hideOversized = false;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.tooltip.label.rotation = 270;
        categoryAxis.tooltip.label.horizontalCenter = "right";
        categoryAxis.tooltip.label.verticalCenter = "middle";
        categoryAxis.title.text = title_axis;
        categoryAxis.title.fontWeight = "bold";


        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = value_title;
        valueAxis.title.fontWeight = "bold";
        if (max_x == 100) {
            valueAxis.max = max_x;
            valueAxis.min = 0;
        }


// Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = "visits";
        series.dataFields.categoryX = "country";
        series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var bullet = series.bullets.push(new am4charts.LabelBullet);
        bullet.label.text = "{visits} " + label;
        if (max_x == 100)
            bullet.label.dy = 20;
        else
            bullet.label.dy = -7;


        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", function (fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
        });

        columnTemplate.adapter.add("stroke", function (stroke, target) {
            return chart.colors.getIndex(target.dataItem.index);
        });

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;

    });
}
