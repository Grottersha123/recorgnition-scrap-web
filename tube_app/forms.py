from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, RadioField, SelectField, BooleanField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired, InputRequired
from wtforms.widgets.html5 import NumberInput

from tube_app.models import steel_grade_query, pipe_size_query, tube_select, tube_select_date, order_select


class TubeForm(FlaskForm):
    name_department = StringField('Цех', validators=[InputRequired()])

    pipe_size = QuerySelectField('Диаметр трубы', query_factory=pipe_size_query, allow_blank=False,
                                 validators=[DataRequired()])

    order_number = IntegerField('Номер партии', widget=NumberInput(),
                                validators=[DataRequired(message="Введите числовое значение")])

    steel_grade = QuerySelectField('Марка стали', query_factory=steel_grade_query,
                                   allow_blank=False,
                                   validators=[InputRequired()], get_label='name')
    manufacturer_name = StringField('Наименование производителя', validators=[InputRequired()])

    submit_button = SubmitField('Подтвердить')


class TubeFormAnalysis(FlaskForm):
    name_department = QuerySelectField('Цех', query_factory=tube_select,
                                       get_label='name_department', validators=[InputRequired()])

    pipe_size = QuerySelectMultipleField('Диаметр трубы', query_factory=pipe_size_query, allow_blank=False,
                                         validators=[InputRequired()])

    order_number = QuerySelectMultipleField('Номер партии', query_factory=order_select, get_label='order_number',
                                            validators=[InputRequired()])

    steel_grade = QuerySelectField('Марка стали', query_factory=steel_grade_query,
                                   allow_blank=False,
                                   validators=[InputRequired()], get_label='name')

    manufacturer_name = QuerySelectMultipleField('Производителя', query_factory=tube_select,
                                                 get_label='manufacturer_name', validators=[InputRequired()])

    date = QuerySelectMultipleField('Дата подсчета', query_factory=tube_select_date,
                                    get_label='date_registration', validators=[InputRequired()])

    group_by_field = RadioField('Группировка по: ', default='order_number',
                                choices=[('order_number', 'Группировка по Номеру партии'),
                                         ('manufacturer_name', 'Группировка по Производителю'),
                                         ('date_registration', 'Группировка по Дате')],
                                validators=[InputRequired()])

    submit_button = SubmitField('Построить график')


class TubeHistory(FlaskForm):
    date = SelectField('', validators=[InputRequired()])


from flask_security.forms import LoginForm


class ExtendedRegisterForm(LoginForm):
    email = StringField('Логин', [InputRequired()])
    password = StringField('Пароль', [InputRequired()])
    remember = BooleanField('Запомнить меня', [InputRequired()])
