from sqlalchemy.dialects.postgresql import JSON

from app import db


class SteelGrade(db.Model):
    __tablename__ = 'steel_grade'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    tube = db.relationship('Tube', backref='steel_grade', lazy=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '[{}]'.format(self.name)


def steel_grade_query():
    return SteelGrade.query


class PipeSize(db.Model):
    __tablename__ = 'pipe_size'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    tube = db.relationship('Tube', backref='pipe_size', lazy=True)

    def __init__(self, name):
        self.name = name

    def get_name(self):
        return '{} мм'.format(self.name)

    def __repr__(self):
        return '{} мм'.format(self.name)


def pipe_size_query():
    return PipeSize.query.order_by("name")


class CountTube(db.Model):
    __tablename__ = 'count_tube'
    id = db.Column(db.Integer, primary_key=True)
    count_tubes = db.Column(db.Integer)
    coordinates = db.Column(JSON)
    name_orig = db.Column(db.String)
    tube_id = db.Column(db.Integer(), db.ForeignKey('tube.id'))

    def __init__(self, **kwargs):
        super(CountTube, self).__init__(**kwargs)

    def __repr__(self):
        return '<id {}-count_tube-{}>'.format(self.id, self.count_tubes)


class Tube(db.Model):
    __tablename__ = 'tube'
    id = db.Column(db.Integer, primary_key=True)
    manufacturer_name = db.Column(db.String)
    name_department = db.Column(db.String)
    order_number = db.Column(db.String)
    steel_id = db.Column(db.Integer(), db.ForeignKey('steel_grade.id'))
    pipe_id = db.Column(db.Integer(), db.ForeignKey('pipe_size.id'))
    date_registration = db.Column(db.String)
    count_tubes = db.relationship('CountTube', backref='count_tube', lazy=True)
    journal_id = db.Column(db.Integer(), db.ForeignKey('journal.id'))

    def __init__(self, **kwargs):
        super(Tube, self).__init__(**kwargs)

    def __repr__(self):
        return '<id {}-{}-{}>'.format(self.id, self.manufacturer_name, self.order_number)


def tube_select():
    return Tube.query.distinct(Tube.manufacturer_name)


def order_select():
    return Tube.query.distinct(Tube.order_number)


def tube_select_date():
    return Tube.query.distinct(Tube.date_registration)


class Journal(db.Model):
    __tablename__ = 'journal'
    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    tubes = db.relationship('Tube', backref='journal', lazy=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))

    def __init__(self, **kwargs):
        super(Journal, self).__init__(**kwargs)

    def __repr__(self):
        return '<id {}-{}>'.format(self.id, self.user_id)
