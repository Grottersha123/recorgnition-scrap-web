import datetime
import json
import os

# TODO:// Под вопрос импорт app ибо вохможно будет цикличность и надо как-то по другому настройки выгружать
import cv2
import keras.backend as k
import numpy as np
from flask import render_template, Blueprint, flash, request, jsonify, Response
from flask_httpauth import HTTPTokenAuth
from flask_security import roles_accepted
from werkzeug.utils import secure_filename

from app import app, db, allowed_file, User, login_required, Role, secrets
from config import TEST_FOLDER_TUBE, test_image_tube
from segmentation_tubes import draw_circles_images, load_data, crop_by_mask_black, include_area, model_compile, \
    find_circles
from tube_app.forms import TubeForm, TubeFormAnalysis, TubeHistory
from tube_app.models import Tube, CountTube, SteelGrade, PipeSize, Journal

test_image_tube = [os.path.join(TEST_FOLDER_TUBE, i) for i in test_image_tube]

# CONFIG
tubes = Blueprint('tube_app', __name__, template_folder='templates')

auth = HTTPTokenAuth(scheme='Bearer')


# tokens = {
#     "secret-token-1": "john",
#     "secret-token-2": "susan"
# }

# @auth.verify_token
# def verify_token(token):
#     if token in tokens:
#         return tokens[token]
#
@auth.verify_token
def verify_token(token):
    user = User.query.filter_by(token=token).first()
    if user:
        return user.token


def get_dict(model):
    res = []
    for u in model:
        res_u = u.__dict__
        del res_u['_sa_instance_state']
        res.append(res_u)
    return res


# TODO Закодировать пароли
# получение токена
@tubes.route('/login_tube', methods=["POST"])
def login_tube():
    if request.method == 'POST':
        # Посылается оригинальная картинка и маска
        login = request.form.get('login', type=str)
        password = request.form.get('password', type=str)
        user = User.query.filter_by(email=login, password_a=password).first()
        if user:
            return jsonify(token=user.token,
                           login=user.email,
                           name=user.name,
                           surname=user.surname,
                           second_name=user.second_name,
                           manufacture_name=user.manufacture_name)
        else:
            status_code = Response(status=401)
            return status_code


# регистрация
@tubes.route('/register_tube', methods=["POST"])
def register_tube():
    if request.method == 'POST':
        login = request.form.get('login', type=str)
        password = request.form.get('password', type=str)
        name = request.form.get('name', type=str)
        surname = request.form.get('surname', type=str)
        manufacture_name = request.form.get('manufacture_name', type=str)
        second_name = request.form.get('second_name', type=str)

        user = User(
            email=login,
            password=password,
            password_a=password,
            active=True,
            name=name,
            surname=surname,
            second_name=second_name,
            manufacture_name=manufacture_name,
            token=secrets.token_urlsafe(20)
        )
        # user_datastore.create_user(email=login,
        #     password=password,
        #     active=True,
        #     name=name,
        #     surname=surname,
        #     second_name=second_name,
        #     manufacture_name=manufacture_name)
        user.roles.append(Role.query.filter_by(name='api tube').first())
        db.session.add(user)
        db.session.commit()
        return jsonify(token=user.token)


@tubes.route('/start_journal', methods=["POST"])
@auth.login_required
def start_journal():
    if request.method == 'POST':
        login = request.form.get('login', type=str)
        journal = Journal(
            user_id=User.query.filter_by(email=login).first().id,
            start=datetime.datetime.now()
        )
        db.session.add(journal)
        db.session.commit()
        return jsonify(journal_id=journal.id)


@tubes.route('/end_journal', methods=["POST"])
@auth.login_required
def end_journal():
    if request.method == 'POST':
        journal_id = request.form.get('journal_id', type=int)
        journal = Journal.query.get(journal_id)
        journal.end = datetime.datetime.now()
        db.session.add(journal)
        db.session.commit()
        status_code = Response(status=200)
        return status_code
    # return jsonify(coordinates=coordinates, count_tube=len(coordinates))


@tubes.route('/get_coordinates_tubes', methods=["POST"])
@auth.login_required
def get_count_tubes():
    if request.method == 'POST':

        # Посылается оригинальная картинка и маска
        file_orig = request.files['file_orig']
        file_mask = request.files['file_mask']

        exclude = request.form.get('exclude', type=bool)
        include = request.form.get('include', type=bool)
        if file_orig.filename == '':
            resp = jsonify({'message': 'No file orig selected for uploading'})
            resp.status_code = 400
            return resp
        if file_orig and allowed_file(file_orig.filename):
            filename = secure_filename(file_orig.filename)

            # print(file.)
            image = cv2.imdecode(np.fromstring(file_orig.read(), np.uint8), cv2.IMREAD_UNCHANGED)
            w, h, c = image.shape
            size = None
            # РАЗМЕР УСТАНОВЛеН
            cv2.imwrite(os.path.join(app.config['TEST_IMAGE_FOLDER_UPLOAD_TUBE'], filename), image)
            if (w, h) != (768, 1024):
                image = cv2.resize(image, (1024, 768))
                size = (h, w)
            if exclude or include:
                image_mask = cv2.imdecode(np.fromstring(file_mask.read(), np.uint8), cv2.IMREAD_UNCHANGED)
                w, h, c = image_mask.shape
                size = None
                if (w, h) != (768, 1024):
                    image_mask = cv2.resize(image_mask, (1024, 768))
                    size = (h, w)
                # далее фигачим если одно то другое и
                # Уже с размерами
                res = include_area(image_mask)
                # лайфхак
                res = cv2.merge((res, res, res))
                # для тестирования ипользую out
                if exclude:
                    image = crop_by_mask_black(image, res, exclude_mask=True)
                if include:
                    image = crop_by_mask_black(image, res, exclude_mask=False)

            model, preprocess_input = model_compile(app.config['MODELS_SEGMENTATION_TUBE'])
            print(app.config['MODELS_SEGMENTATION_TUBE'])
            image = preprocess_input(image)
            coordinates, tresh = find_circles(image, model, size=size)
            k.clear_session()
        return jsonify(coordinates)


@tubes.route('/get_description_tubes', methods=["GET"])
@auth.login_required
def get_description_tube():
    if request.method == 'GET':
        steel_grade = SteelGrade.query.all()
        pipe_size = PipeSize.query.all()
        steel_grade = get_dict(steel_grade)
        pipe_size = get_dict(pipe_size)
        return jsonify(pipe_info=pipe_size, steel_info=steel_grade)


@tubes.route('/post_info_tubes', methods=["POST"])
@auth.login_required
def post_info_tubes():
    if request.method == 'POST':
        manufacturer_name = request.form.get('manufacturer_name', type=str)
        order_number = request.form.get('order_number', type=int)
        name_department = request.form.get('name_department', type=str)
        steel_id = request.form.get('steel_id', type=int)
        pipe_id = request.form.get('pipe_id', type=int)
        journal_id = request.form.get('journal_id', type=int)
        print(journal_id)
        tube = Tube(
            manufacturer_name=manufacturer_name,
            order_number=order_number,
            name_department=name_department,
            steel_id=steel_id,
            pipe_id=pipe_id,
            journal_id=journal_id,
            date_registration=datetime.datetime.now().date())

        db.session.add(tube)
        db.session.commit()
        return jsonify(tube_id=tube.id)


# Сохранение данных в бд при нажатии кнопки сохранить
@tubes.route('/post_count_tubes', methods=["POST"])
@auth.login_required
def post_count_tubes():
    if request.method == 'POST':
        # # Посылается оригинальная картинка и маска
        file_orig = request.files['file_orig']
        if file_orig.filename == '':
            resp = jsonify({'message': 'No file orig selected for uploading'})
            resp.status_code = 400
            return resp
        if file_orig and allowed_file(file_orig.filename):
            image = cv2.imdecode(np.fromstring(file_orig.read(), np.uint8), cv2.IMREAD_UNCHANGED)
            filename = secure_filename(file_orig.filename)
            cv2.imwrite(os.path.join(app.config['TEST_IMAGE_FOLDER_UPLOAD_TUBE'], filename), image)
        count_tubes = request.form.get('count_tubes', type=int)
        coordinates = request.form.get('coordinates', type=int)
        tube_id = request.form.get('tube_id', type=int)
        journal_id = request.form.get('journal_id', type=int)
        # TODO поменять если что название файла убрать
        count_tube = CountTube(
            count_tubes=count_tubes,
            coordinates=coordinates,
            # name_orig=name_orig,
            name_orig=os.path.join(app.config['TEST_FOLDER_UPLOAD_TUBE'], filename),
            tube_id=tube_id,
        )
        print(journal_id)
        journal = Journal.query.get(journal_id)
        print(journal)
        journal.end = datetime.datetime.now()

        db.session.add(count_tube, journal)
        db.session.commit()
        return jsonify(count_tube=count_tube.id)
    # return jsonify(coordinates=coordinates, count_tube=len(coordinates))


# Конец АPI
# TODO: Поправить проблему  с тем что форма затирается с данными.

@tubes.route('/count_tubes', methods=["GET", "POST"])
@login_required
@roles_accepted('admin', 'tube')
def count_tubes():
    title = 'Подсчет количества труб'

    form = TubeForm()
    if form.validate_on_submit():
        flash('Данные по трубам добавлены. \n Выберите фотографии')
        print(form.pipe_size.data.id)
        tube_new = Tube(
            manufacturer_name=form.manufacturer_name.data,
            order_number=form.order_number.data,
            name_department=form.name_department.data,
            steel_id=form.steel_grade.data.id,
            pipe_id=form.pipe_size.data.id,
            date_registration=datetime.datetime.now().date())
        db.session.add(tube_new)
        db.session.commit()
        return render_template('tube_template/tubes.html', title=title, images_test=test_image_tube,
                               tube_id=tube_new.id)
    if request.method == 'POST':
        k.clear_session()
        batch_images = request.form.getlist('image[]')
        tube_id = request.form.get('tube_id')
        chose_image = [os.path.split(i)[-1] for i in batch_images]
        coordinates = load_data(chose_image, app.config['TEST_IMAGE_FOLDER_TUBE'],
                                path_model=app.config['MODELS_SEGMENTATION_TUBE'])

        result = draw_circles_images(coordinates)
        tube_new = Tube.query.get(tube_id)
        len_tubes = []
        for index, im in enumerate(coordinates):
            len_tubes.append(len(im[0]))
            count_tube = CountTube(
                count_tubes=len(im[0]),
                coordinates=im[0],
                name_orig=im[1],
                tube_id=tube_id)
            db.session.add(count_tube)
        db.session.commit()
        # cleare keras cash for recorgnizer
        k.clear_session()
        print(len_tubes)
        return render_template('tube_template/tubes.html', title=title, images_test=test_image_tube,
                               len_tubes=len_tubes, images=result, tube_id=tube_id)
    if request.method == 'GET':
        return render_template('tube_template/submit_tube.html', form=form, title=title)


title_graph = {'order_number': 'Номер партии', 'manufacturer_name': 'Наименование производителя',
               'date_registration': 'Дата добавления'}


# def select_groub_by_count_tube(name, lst_order_number, lst_manufacture_name, lst_date, lst_pipe_size):
#     result = db.session.execute(
#         'select {name}, sum(count_tubes) '
#         'from tube join count_tube ct on tube.id = ct.tube_id where '
#         'order_number in :lst_number and '
#         'manufacturer_name in :lst_manufacture and '
#         'date_registration in :lst_date and '
#         'pipe_id in :lst_pipe_size '
#         'group by {name} '
#         'order by {name}'.format(name=name),
#         {'lst_number': lst_order_number, 'lst_manufacture': lst_manufacture_name, 'lst_date': lst_date,
#          'lst_pipe_size': lst_pipe_size})
#     print(title_graph[name])
#     return result, title_graph[name]

def select_groub_by_count_tube(name, lst_order_number, lst_manufacture_name, lst_date, lst_pipe_size):
    result = db.session.execute(
        'select {name}, sum(count_tubes) '
        'from tube join count_tube ct on tube.id = ct.tube_id where '
        'order_number in :lst_number and '
        'manufacturer_name in :lst_manufacture and '
        'date_registration in :lst_date and '
        'pipe_id in :lst_pipe_size '
        'group by {name} '
        'order by {name}'.format(name=name),
        {'lst_number': lst_order_number, 'lst_manufacture': lst_manufacture_name, 'lst_date': lst_date,
         'lst_pipe_size': lst_pipe_size})
    print(title_graph[name])
    return result, title_graph[name]


# TODO: добавиьб аналитику по тому что было выбрано в графиках
@tubes.route('/dash_tube', methods=["GET", "POST"])
@login_required
@roles_accepted('admin', 'tube')
def dash_board():
    title = 'Подсчет количества труб'
    form = TubeFormAnalysis()
    # if form.validate_on_submit():
    #     lst_order_number = [i for i in form.order_number.raw_data]
    #
    #     print(lst_order_number)
    #     result = db.session.execute(
    #         'select order_number , sum(count_tubes) from tube join count_tube ct on tube.id = ct.tube_id where '
    #         'order_number in lst_number '
    #         'group by order_number',
    #         {'lst_number': form.order_number.data})
    #     return render_template('tube_analytic.html', form=form, title=title)
    if request.method == 'POST':
        lst_order_number = tuple([i.order_number for i in form.order_number.data])
        lst_manufature_name = tuple([i.manufacturer_name for i in form.manufacturer_name.data])
        lst_date = tuple([i.date_registration for i in form.date.data])
        lst_pipe_size = tuple([i.id for i in form.pipe_size.data])
        result, title_for_graph = select_groub_by_count_tube(form.group_by_field.data, lst_order_number,
                                                             lst_manufature_name, lst_date, lst_pipe_size)
        results = [i for i in result]
        graphs_3d = [dict(country=cls, visits=int(res)) for cls, res in results]
        return render_template('tube_template/tube_analytic.html', form=form, title=title,
                               title_for_graph=title_for_graph,
                               graphJSONAverage3d=json.dumps(graphs_3d))
    return render_template('tube_template/tube_analytic.html', form=form, title=title)


def sum_tubes(tubes):
    return sum([t.count_tubes for t in tubes])


# АPI ДЛЯ Труб
@tubes.route('/_get_history/')
def _get_senders():
    user_id = request.args.get('user_id', '', type=str)
    res = User.query.filter_by(login=user_id).first()
    return jsonify([])


@tubes.route('/history_tube/<login>', methods=["GET"])
@login_required
def history_tube(login):
    user_id = User.query.filter_by(email=login).first_or_404().id
    journal = Journal.query.filter_by(user_id=user_id)
    print(journal)
    data = db.session.execute('select  start, sum(ct.count_tubes), t.order_number  '
                              'from journal left join tube t on journal.id = t.journal_id '
                              'left join count_tube ct on t.id = ct.tube_id  where user_id={user_id}'
                              'group by order_number, start order by start'.format(user_id=user_id))

    data = [d for d in data]
    values = set(map(lambda x: x[0], data))
    data_res = [[['0', 0, '0'] if None in list(i) else [i[2], i[1], i[2]] for i in data if i[0] == x] for x in values]
    # data = [[[i.order_number, sum_tubes(i.count_tubes), i.order_number] for i in j.tubes] for j in journal]
    # data_res = [[[i.order_number, sum_tubes(i.count_tubes), i.order_number] for i in j.tubes] for j in journal]

    form = TubeHistory()
    form.date.choices = [(index, a.start.strftime("%Y-%m-%d %H:%M:%S")) for index, a in enumerate(journal)]
    # form.date.choices = [(index, a[0]) for index, a in enumerate(data)]
    # form.date.choices = [(index, a[0].strftime("%Y-%m-%d %H:%M:%S")) for index, a in enumerate(data)]
    form.date.choices.sort(key=lambda tup: tup[1])
    print(form.date.choices)
    if form.date.choices:
        start_date = form.date.choices[0][-1].split(' ')[0]
    return render_template('tube_template/tube_history.html', login=login, form=form, date_select=start_date,
                           current_row=data_res[form.date.choices[0][0]], data=data_res)


# TODO:// поправить ошибку с order_number c str на int
@tubes.route('/history_tube/<login>/gallery/<int:tube_id>', methods=["GET"])
@login_required
def gallery_tube(login, tube_id):
    # print(user_id)
    # user_id = User.query.filter_by(email=user_id).first_or_404().id
    # journal = Journal.query.filter_by(user_id=user_id)
    # data = [[[i.order_number, sum_tubes(i.count_tubes)] for i in j.tubes] for j in journal]
    # form = TubeHistory()
    # form.date.choices = [(index, a.start) for index, a in enumerate(journal)]
    # form.date.choices.sort(key=lambda tup: tup[1])
    # if form.date.choices:
    #     start_date = form.date.choices[0][-1].split(' ')[0]
    print([i for i in Tube.query.filter_by(order_number=str(tube_id))])
    tube = db.session.execute(
        'select name_orig, order_number, name_department, manufacturer_name, ct.count_tubes, st.name, p.name  '
        'from tube '
        'left join steel_grade st on tube.steel_id=st.id '
        'left join pipe_size p on tube.pipe_id=p.id '
        'left join count_tube ct on tube.id = ct.tube_id'
        ' where order_number=\'{order_num}\''.format(order_num=tube_id))
    tube = [list(t) for t in tube]
    # images = [[t[0].name_orig[0], t[0].count_tubes] for t in tube]
    return render_template('tube_template/tube_gallery.html', images=tube, number_order=tube_id,
                           login=login, count_photo=len(tube))
